#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NOM 35
#define MAX_COGNOM 35
#define MAX_ASSIGNATURA 35
#define BB while(getchar()!='\n')

struct Clase{
    char nom[MAX_NOM+1];
    char cognom[MAX_COGNOM+1];
    char assignatura[MAX_ASSIGNATURA+1];
    int nota;
};

void entradaClase(struct Clase *c);
void imprimirClase(struct Clase c);
int ecriureClaseDisc(FILE *fitxer,struct Clase *c, int nReg);
int llegirClaseDisc(FILE *fitxer,struct Clase *c, int nReg);
void llistatPantalla(FILE *fitxer);
void llistatPaginaWeb(FILE *fitxer);

int main()
{
    struct Clase c;
    FILE *fitxer;
    int opcio;
    char mes;

    do {
        printf("\n**** MENU ****\n");
        printf("1. Alta\n");
        printf("2. Llistat per pantalla\n");
        printf("3. Llistat en pagina web\n");
        printf("\n9. Sortir\n");
        printf("Tria opcio (1-3): ");
        scanf("%d", &opcio); BB;


        switch(opcio) {
            case 1:
                do {
                    fitxer = fopen("clase.bin", "ab");
                    if(fitxer == NULL) {
                        printf("Error obrint el fitxer.\n");
                        exit(1);
                    }
                    entradaClase(&c);
                    ecriureClaseDisc(fitxer, &c, 1);
                    fclose(fitxer);
                    printf("Vols donar d'alta a mes (s/n)? ");
                    scanf(" %c", &mes); BB;
                } while(mes == 's' || mes == 'S');
                break;
            case 2:
                generallistapantalla();
                break;
            case 3:
                fitxer = fopen("clase.bin", "rb");
                if(fitxer == NULL) {
                    printf("Error obrint el fitxer.\n");
                    exit(1);
                }
                llistatPaginaWeb(fitxer);
                fclose(fitxer);
                break;
            case 9:
                printf("Sortint del programa...\n");
                break;
            default:
                printf("Opcio no valida.\n");
        }

    } while(opcio != 9);

    return 0;
}

void entradaClase(struct Clase *c){
    printf("Nom: ");
    scanf("%35[^\n]",c->nom);BB;
    printf("Cognom: ");
    scanf("%35[^\n]",c->cognom);BB;
    printf("Assignatura: ");
    scanf("%35[^\n]",c->assignatura);BB;
    printf("Nota: ");
    scanf("%i",&c->nota);BB;
}

void imprimirClase(struct Clase c){
    printf("Nom: %s\n",c.nom);
    printf("Cognom: %s\n",c.cognom);
    printf("Assignatura: %s\n",c.assignatura);
    printf("Nota: %i\n\n", c.nota);


}

//aquesta accio genera tots les Persones qui hi ha al meu fitxer per patalla
int generallistapantalla(){
FILE *fitxer;
int error,n;
struct Clase c;
int i=1;
printf("\n***LECTURA DE LES CLASSES DEL FITXER****\n");
//obrir llegir escriptura
fitxer= fopen("clase.bin","rb");//fitxer binari (b) d'afegir (a)
if(fitxer==NULL){
error=1; //error obrir fitxer
}
if(feof(fitxer))printf("no hi ha cap registre Al Fitxer\n");
while(!feof(fitxer)){
n=llegirClaseDisc(fitxer, &c, 1);
if(n==0){


printf("******Dades de les Classes '%i'  \n",i);
imprimirClase(c);
i++;


}
if(n==1){
printf("\nError en obrir fitxer...\n");
break;
}
if(n==3){
printf("\nError de lectura...\n");
break;
}
}
if(fclose(fitxer)){
printf("error al tancar el fitxer...");
}
return error;
}


int ecriureClaseDisc(FILE *fitxer,struct Clase *c, int nReg){
    int n,error=0;
    n=fwrite(c,sizeof(struct Clase),nReg,fitxer);
    if(n!=nReg){
        error=2; //error numero registres escrits
    }
    return error;
}

int llegirClaseDisc(FILE *fitxer,struct Clase *c, int nReg){
    int n,error=0;
    n=fread(c,sizeof(struct Clase),nReg,fitxer);
    if(!feof(fitxer)){
        if(n==0){
            error=3; //error de lectura
        }
    }else{
        error=4 ; //�s un av�s. Ha arribat al final del fitxer.
    }
    return error;
}


void llistatPaginaWeb(FILE *fitxer) {
    FILE *web;
    struct Clase c;
    web = fopen("llistat.html", "w");
    if(web == NULL) {
        printf("Error obrint el fitxer de la pagina web.\n");
        exit(1);
    }
    fprintf(web, "<html>\n<head>\n<title>Llistat de les classes</title>\n</head>\n<body>\n");
    fprintf(web, "<h1>Llistat de les classes</h1>\n");
    fprintf(web, "<table border=\"1\">\n<tr><th>Nom</th><th>Cognom</th><th>Assignatura</th><th>Nota</th></tr>\n");
    while(!feof(fitxer)){
        if(llegirClaseDisc(fitxer, &c, 1) == 0){
            fprintf(web, "<tr><td>%s</td><td>%s</td><td>%s</td><td>%i</td></tr>\n", c.nom,c.cognom, c.assignatura, c.nota);
        }
    }
    fprintf(web, "</table>\n</body>\n</html>");
    fclose(web);
    printf("Pagina web generada amb exit.\n");
}


